package hust.soict.globalict.lab02;

import java.util.Arrays;
import java.util.Random;
public class MatricesAdd {
    public static void main(String[] args){
        int[][] mat1 = new int[4][4];
        int[][] mat2 = new int[4][4];
        int[][] matSum = new int[4][4];
        Random r = new Random();
        for(int i = 0; i < mat1.length; i++){
            for(int j = 0; j < mat1[0].length; j++){
                mat1[i][j] = 10 + r.nextInt(40);
                mat2[i][j] = 10 + r.nextInt(40);
                matSum[i][j] = mat1[i][j] + mat2[i][j];
            }
        }
        System.out.println("Matrix 1:\t\t" + Arrays.deepToString(mat1));
        System.out.println("Matrix 2:\t\t" + Arrays.deepToString(mat2));
        System.out.println("Sum of two matrices:\t" + Arrays.deepToString(matSum));
    }
    
}
