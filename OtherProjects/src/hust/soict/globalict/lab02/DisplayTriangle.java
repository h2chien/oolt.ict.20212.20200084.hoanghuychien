package hust.soict.globalict.lab02;

import java.util.Scanner;
public class DisplayTriangle {
    public static void main(String[] args){
        try (Scanner number = new Scanner(System.in)) {
			System.out.println("Input n: ");
			int n = number.nextInt();
			for(int i = 1; i <= n; i++){
			    for(int j = n-i; j > 0; j--){
			        System.out.print(" ");
			    }
			    for(int k = 0; k < 2*i-1; k++){
			        System.out.print("*");
			    }
			    System.out.println("");
			}
		}
    }
}
