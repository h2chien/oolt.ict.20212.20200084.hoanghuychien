package hust.soict.globalict.lab02;

import java.util.Arrays;
import java.util.Random;

public class ArraySort {
    public static void main(String[] args){
        int[] arr = new int[10];
        Random r = new Random();
        for(int i = 0; i < arr.length; i++){
            arr[i] = 1000 + r.nextInt(3000);
        }
        System.out.println("Before sort: " + Arrays.toString(arr));
        Arrays.sort(arr);
        System.out.println("After sort: " + Arrays.toString(arr));
        int sum = Arrays.stream(arr).sum();
        System.out.println("Sum: " + sum);
        double ave = sum*1.0/arr.length;
        System.out.println("Average: " + ave);
    }
}
