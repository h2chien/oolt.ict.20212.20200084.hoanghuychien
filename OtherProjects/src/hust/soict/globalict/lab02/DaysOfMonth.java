package hust.soict.globalict.lab02;

import java.util.Scanner;
import java.util.Arrays;
import java.util.InputMismatchException;

public class DaysOfMonth {
    public static void main(String[] args){
        try (Scanner input = new Scanner(System.in)) {
			String m = "";
			int year = -1;
			boolean check = false;
			String[] monthNames = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12",
			                    "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December",
			                    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec",
			                    "Jan.", "Feb.", "Mar.", "Apr.", "May", "June", "July", "Aug.", "Sep.", "Oct.", "Nov.", "Dec."
			                    };
			do{
			    System.out.println("Input month: ");
			    try{
			        String month = input.nextLine();
			        if(Arrays.stream(monthNames).anyMatch(s -> s.equals(month)) == false) throw new IllegalArgumentException();
			        else{
			            for(int i = 0; i < monthNames.length; i++){
			                if(monthNames[i].equals(month)){
			                    m = monthNames[i % 12];
			                    break;
			                }
			            }
			        }
			        check = true;
			    }catch(IllegalArgumentException ex){
			        System.out.println("Invalid input for month!");
			    }
			}while(!check);

			check = false;
			do{
			    System.out.println("Input year: ");
			    try {
			        year = input.nextInt();
			        if(year < 1) throw new IllegalArgumentException();
			        check = true;
			    }catch(InputMismatchException | IllegalArgumentException ex){
			        System.out.println("Invalid input for year!");
			        input.nextLine();
			    }
			}while(!check);
			
			switch(Integer.valueOf(m)){
			    case 2:
			        if(year % 100 == 0 && year % 400 != 0) System.out.println("28");
			        else if(year % 4 == 0) System.out.println("29");
			        else System.out.println("28");
			        break;
			    case 4, 6, 9, 11:
			        System.out.println("30");
			        break;
			    default:
			        System.out.println("31");
			        break;
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}