package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
public class Calculator {
    public static void main(String[] args){
        String strNum1, strNum2;
        String strNotification = "Sum: ";
        strNum1 = JOptionPane.showInputDialog(null, "Please input the first number:", "Input the first number", JOptionPane.INFORMATION_MESSAGE);
        double num1 = Double.parseDouble(strNum1);
        strNum2 = JOptionPane.showInputDialog(null, "Please input the second number:", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
        double num2 = Double.parseDouble(strNum2);
        if(num2 == 0){
            JOptionPane.showMessageDialog(null, "Divisor cannot be 0!", "Invalid number", JOptionPane.ERROR_MESSAGE);
            strNum2 = JOptionPane.showInputDialog(null, "Please input the second number:", "Input the second number", JOptionPane.INFORMATION_MESSAGE);
            num2 = Double.parseDouble(strNum2);
        }
        double sum = num1 + num2;
        double diff = num1 - num2;
        double prod = num1*num2;
        double quot = num1/num2;
        strNotification += sum + "\nDifference: " + diff + "\nProduct: " + prod + "\nQuotient: " + quot;
        JOptionPane.showMessageDialog(null, strNotification, "Show two numbers", JOptionPane.INFORMATION_MESSAGE);
		System.exit(0);
    }
}
