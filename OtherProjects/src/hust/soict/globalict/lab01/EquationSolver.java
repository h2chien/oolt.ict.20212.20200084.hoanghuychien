package hust.soict.globalict.lab01;

import javax.swing.JOptionPane;
public class EquationSolver {
    public static void main(String[] args){
        String option;
        option = JOptionPane.showInputDialog(null, "1. First-degree equation\n2. System of two first-degree equations\n3. Second-degree equation" , "Choose what to solve", JOptionPane.INFORMATION_MESSAGE);
        double choice = Double.parseDouble(option);
        if(choice == 1){
            String num1, num2;
            num1 = JOptionPane.showInputDialog(null, "Input a: ", "Equation: a.x + b = 0", JOptionPane.INFORMATION_MESSAGE);
            double a = Double.parseDouble(num1);
            num2 = JOptionPane.showInputDialog(null, "Input b: ", "Equation: a.x + b = 0", JOptionPane.INFORMATION_MESSAGE);
            double b = Double.parseDouble(num2);
            if(a == 0){
                if(b == 0) JOptionPane.showMessageDialog(null, "Equation has infinite solution.", "Result", JOptionPane.INFORMATION_MESSAGE);
                else JOptionPane.showMessageDialog(null, "Equation has no solution.", "Result", JOptionPane.INFORMATION_MESSAGE);
            }
            else{
                double solution = -b/a;
                JOptionPane.showMessageDialog(null, "Equation has one solution: x = " + solution, "Result", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        else if(choice == 2){
            String num1, num2, num3, num4, num5, num6;
            num1 = JOptionPane.showInputDialog(null, "Input a1: ", "Equation: a1.x1 + b1.x2 = c1", JOptionPane.INFORMATION_MESSAGE);
            double a1 = Double.parseDouble(num1);
            num2 = JOptionPane.showInputDialog(null, "Input b1: ", "Equation: a1.x1 + b1.x2 = c1", JOptionPane.INFORMATION_MESSAGE);
            double b1 = Double.parseDouble(num2);
            num3 = JOptionPane.showInputDialog(null, "Input c1: ", "Equation: a1.x1 + b1.x2 = c1", JOptionPane.INFORMATION_MESSAGE);
            double c1 = Double.parseDouble(num3);
            num4 = JOptionPane.showInputDialog(null, "Input a2: ", "Equation: a2.x1 + b2.x2 = c2", JOptionPane.INFORMATION_MESSAGE);
            double a2 = Double.parseDouble(num4);
            num5 = JOptionPane.showInputDialog(null, "Input b2: ", "Equation: a2.x1 + b2.x2 = c2", JOptionPane.INFORMATION_MESSAGE);
            double b2 = Double.parseDouble(num5);
            num6 = JOptionPane.showInputDialog(null, "Input c2: ", "Equation: a2.x1 + b2.x2 = c2", JOptionPane.INFORMATION_MESSAGE);
            double c2 = Double.parseDouble(num6);
            double D = a1*b2 - a2*b1;
            double D1 = c1*b2 - c2*b1;
            double D2 = a1*c2 - a2*c1;
            if(D != 0){
                double x1 = D1/D;
                double x2 = D2/D;
                JOptionPane.showMessageDialog(null, "System has a unique solution: (x1, x2) = (" + x1 + ", " + x2 + ").", "Result", JOptionPane.INFORMATION_MESSAGE);
            }
            if(D == 0){
                if(D1 == 0 && D2 == 0) JOptionPane.showMessageDialog(null, "System has infinitely many solutions.", "Result", JOptionPane.INFORMATION_MESSAGE);
                else JOptionPane.showMessageDialog(null, "System has no solution.", "Result", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        else if(choice == 3){
            String num1, num2, num3;
            num1 = JOptionPane.showInputDialog(null, "Input a: ", "Equation: a.x^2 + b.x + c = 0", JOptionPane.INFORMATION_MESSAGE);
            double a = Double.parseDouble(num1);
            if(a == 0){
                JOptionPane.showMessageDialog(null, "a cannot be 0", "Error!", JOptionPane.ERROR_MESSAGE);
                num1 = JOptionPane.showInputDialog(null, "Input a: ", "Equation: a.x^2 + b.x + c = 0", JOptionPane.INFORMATION_MESSAGE);
                a = Double.parseDouble(num1);
            }
            num2 = JOptionPane.showInputDialog(null, "Input b: ", "Equation: a.x^2 + b.x + c = 0", JOptionPane.INFORMATION_MESSAGE);
            double b = Double.parseDouble(num2);
            num3 = JOptionPane.showInputDialog(null, "Input c: ", "Equation: a.x^2 + b.x + c = 0", JOptionPane.INFORMATION_MESSAGE);
            double c = Double.parseDouble(num3);
            double Delta = b*b - 4*a*c;
            if(Delta == 0){
                double root = -b/(2*a);
                JOptionPane.showMessageDialog(null, "Equation has double root: x = " + root, "Result",JOptionPane.INFORMATION_MESSAGE);
            }
            else if(Delta > 0){
                double root1 = (-b + Math.sqrt(Delta))/(2*a);
                double root2 = (-b - Math.sqrt(Delta))/(2*a);
                JOptionPane.showMessageDialog(null, "Equation has two distinct roots:\nx1 = " + root1 + "\nx2 = " + root2, "Result", JOptionPane.INFORMATION_MESSAGE);
            }
            else JOptionPane.showMessageDialog(null, "Equation has no solution.", "Result", JOptionPane.INFORMATION_MESSAGE);
        }
        else JOptionPane.showMessageDialog(null, "Invalid option!", "Error!", JOptionPane.ERROR_MESSAGE);
        System.exit(0);
    }
}
