package hust.soict.globalict.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {
	private static final int TIME_LIMIT = 10000;
	
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("garbage.txt");
		try (Scanner scanner = new Scanner(file)) {
			@SuppressWarnings("unused")
			String s = "";
			long start = System.currentTimeMillis();
			while(scanner.hasNextLine() && System.currentTimeMillis() - start < TIME_LIMIT) {
				s += scanner.nextLine();
			}
			System.out.println(System.currentTimeMillis() - start);
			if(System.currentTimeMillis() - start == TIME_LIMIT) System.out.println("Too much garbage!");
		}
	}
}
