package hust.soict.globalict.garbage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.UUID;

public class GarbageFileGenerator {
	
	private static final int MAX_NUMBER_LINES = 100000;

	public static void main(String[] args) throws IOException {
		Path file = Path.of("garbage.txt");
		Files.writeString(file, "");
		for(int i = 0; i < MAX_NUMBER_LINES; i++) {
			Files.writeString(file, generateString(), StandardOpenOption.APPEND);
		}
    }

    public static String generateString() {
        String uuid = UUID.randomUUID().toString() + "\n";
        return uuid.replaceAll("-", "");
    }

}
