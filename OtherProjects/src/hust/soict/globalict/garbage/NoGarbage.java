package hust.soict.globalict.garbage;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NoGarbage {
	public static void main(String[] args) throws FileNotFoundException {
		File file = new File("garbage.txt");
		try (Scanner scanner = new Scanner(file)) {
			long start = System.currentTimeMillis();
			@SuppressWarnings("unused")
			String s = "";
			StringBuilder sb = new StringBuilder();
			while(scanner.hasNextLine()) {
				sb.append(scanner.nextLine());
			}
			s = sb.toString();
			System.out.println(System.currentTimeMillis() - start);
		}
	}
}
