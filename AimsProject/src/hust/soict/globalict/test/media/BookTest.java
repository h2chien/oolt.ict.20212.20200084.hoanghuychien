package hust.soict.globalict.test.media;

import hust.soict.globalict.aims.media.Book;

public class BookTest {
	public static void main(String[] args) {
		Book book = new Book("ONE-PUNCH MAN", "Comic", 20000);
		book.addAuthor("ONE");
		book.addAuthor("Yusuke Murata");
		book.setContent("Manga, R16, OPM, Saitama, Bald, Strongest");
		System.out.println(book.toString());
	}
}
