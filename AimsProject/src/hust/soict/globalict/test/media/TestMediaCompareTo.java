package hust.soict.globalict.test.media;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Track;

public class TestMediaCompareTo {
	public static void main(String[] args) {
		List<DigitalVideoDisc> dvd = new ArrayList<DigitalVideoDisc>();
		
		DigitalVideoDisc dvd1 = new DigitalVideoDisc("The Lion King", "Animation",  19.95f);
	    DigitalVideoDisc dvd2 = new DigitalVideoDisc("Star Wars", "Science Fiction",  24.95f);
	    DigitalVideoDisc dvd3 = new DigitalVideoDisc("Aladdin", "Animation", 18.99f);
	    
	    dvd.add(dvd1);
	    dvd.add(dvd2);
	    dvd.add(dvd3);
	    
	    Iterator<DigitalVideoDisc> dvdIterator = dvd.iterator();
	    
	    System.out.println("DVD before sort:\n");
	    while(dvdIterator.hasNext()) {
	    	System.out.println(dvdIterator.next().getTitle());
	    }
	    
	    System.out.println("\nDVD after sort:\n");
	    Collections.sort(dvd);
	    dvdIterator = dvd.iterator();
	    while(dvdIterator.hasNext()) {
	    	System.out.println(dvdIterator.next().getTitle());
	    }
	    
	    List<CompactDisc> cd = new ArrayList<CompactDisc>();
	    cd.add(new CompactDisc("C"));
	    cd.get(0).addTrack(new Track("C2", 2));
	    cd.get(0).addTrack(new Track("C1", 2));
	    cd.add(new CompactDisc("B"));
	    cd.get(1).addTrack(new Track("B2", 2));
	    cd.get(1).addTrack(new Track("B1", 2));
	    cd.add(new CompactDisc("D"));
	    cd.get(2).addTrack(new Track("D1", 1));
	    cd.add(new CompactDisc("A"));
	    cd.get(3).addTrack(new Track("A1", 2));
	    
	    Iterator<CompactDisc> cdIterator = cd.iterator();
	    
	    System.out.println("\nCD before sort:\n");
	    while(cdIterator.hasNext()) {
	    	cdIterator.next().play();
	    }
	    
	    Collections.sort(cd);
	    Collections.sort(cd.get(0).getTracks());
	    Collections.sort(cd.get(1).getTracks());
	    Collections.sort(cd.get(2).getTracks());
	    Collections.sort(cd.get(3).getTracks());
	    cdIterator = cd.iterator();
	    System.out.println("\nCD after sort:\n");
	    while(cdIterator.hasNext()) {
	    	cdIterator.next().play();
	    }
	}
}
