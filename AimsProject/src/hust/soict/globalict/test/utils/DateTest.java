package hust.soict.globalict.test.utils;
import hust.soict.globalict.aims.utils.DateUtils;
import hust.soict.globalict.aims.utils.MyDate;

public class DateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MyDate day1 = new MyDate("third", "January", "two thousand");
		
		MyDate day2 = new MyDate("sixteenth", "April", "eighteen hundred");
		
		MyDate day3 = new MyDate("thirty-first", "May", "two thousand and four");
		
		MyDate day4 = new MyDate("twenty sixth", "July", "nineteen ninety-nine");
		
		MyDate day5 = new MyDate("twenty-second", "August", "sixteen O nine");
		
		MyDate day6 = new MyDate("fifteenth", "May", "twenty twenty-two");
		
		System.out.println("Before sorting:");
		day1.print();
		day2.print("yyyy-MM-dd");
		day3.print("d/M/yyyy");
		day4.print("dd-MMM-yyyy");
		day5.print("MMM d yyyy");
		day6.print("MM-dd-yyyy");
		
		MyDate[] dates = new MyDate[6];
		dates[0] = day1;
		dates[1] = day2;
		dates[2] = day3;
		dates[3] = day4;
		dates[4] = day5;
		dates[5] = day6;
		
		System.out.println("\nAfter sorting:");
		DateUtils.sortDates(dates);
	}

}
