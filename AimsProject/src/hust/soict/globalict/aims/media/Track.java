package hust.soict.globalict.aims.media;

public class Track implements Playable, Comparable<Object> {
	private String title;
	private int length;
	
	public String getTitle() {
		return title;
	}
	public int getLength() {
		return length;
	}
	
	public Track(String title, int length) {
		super();
		this.title = title;
		this.length = length;
	}
	
	public Track(String title) {
		this(title, 0);
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Track: " + this.getTitle());
		System.out.println("Track length: " + this.getLength() + "s");
	}
	
	@Override
	public boolean equals(Object o) {
		Track track = (Track)o;
		if(this.getTitle().equals(track.getTitle()) && this.getLength() == track.getLength()) {
			return true;
		}
		return false;
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Track track = (Track)o;
		return this.getTitle().compareTo(track.getTitle());
	}
	
}
