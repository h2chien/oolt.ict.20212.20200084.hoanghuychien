package hust.soict.globalict.aims.media;

public abstract class Media implements Comparable<Object>{
	private String title;
	private String category;
	private float cost;
	private String id = "";
	private boolean lucky;
	
	public Media(String title){
		this.title = title;
	}
		
	public Media(String title, String category){
		this(title);
		this.category = category;
	}
	
	public Media(String title, String category, float cost) {
		this.title = title;
		this.category = category;
		this.cost = cost;
	}

	public String getTitle() {
		return title;
	}

	public String getCategory() {
		return category;
	}
	
	public float getCost() {
		return cost;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public boolean isLucky() {
		return lucky;
	}

	public void setLucky(boolean lucky) {
		this.lucky = lucky;
	}
	
	@Override
	public boolean equals(Object o) {
		Media m = (Media)o;
		return this.getId().equals(m.getId());
	}
	
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Media media = (Media)o;
		if(this.getCost() == media.getCost()) return this.getTitle().compareTo(media.getTitle());
		return ((Float)this.getCost()).compareTo(media.getCost());
	}
}
