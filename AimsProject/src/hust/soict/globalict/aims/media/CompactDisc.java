package hust.soict.globalict.aims.media;

import java.util.ArrayList;

public class CompactDisc extends Disc implements Playable, Comparable<Object>{
	private String artist;

	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public CompactDisc(String title, String category, float cost, String artist, ArrayList<Track> tracks) {
		super(title, category, cost);
		this.artist = artist;
		this.tracks = tracks;
	}

	public String getArtist() {
		return artist;
	}
	
	public void addTrack(Track track) {
		if(!tracks.contains(track)) {
			tracks.add(track);
			this.setLength(this.getLength() + track.getLength());
			System.out.println("Track has added!");
		}else System.out.println("Already on list!");
	}

	public void removeTrack(Track track) {
		if(tracks.contains(track)) {
			for(int i = 0; i < tracks.size(); i++) {
				if(tracks.get(i).equals(track)) { 
					tracks.remove(i);
					this.setLength(this.getLength() - track.getLength());
					System.out.println("Have removed!");
				}
			}
		}else {
			System.out.println("Not on list!");
		}
	}
	
	public ArrayList<Track> getTracks() {
		return tracks;
	}
	
	@Override
	public int getLength() {
		int length = 0;
		for(Track track: tracks) {
			length += track.getLength();
		}
		return length;
	}
	
	@Override
	public void play() {
		// TODO Auto-generated method stub
		System.out.println("Playing CD: " + this.getTitle());
		System.out.println("CD length: " + this.getLength() + "s");
		System.out.println("CD contains:");
		for(Track track: tracks) {
			track.play();
		}
		System.out.println();
	}
	
	@Override
	public int compareTo(Object o) {
		CompactDisc cd = (CompactDisc)o;
		if(this.getTracks().size() == cd.getTracks().size()) {
			if(this.getLength() == cd.getLength()) return this.getTitle().compareTo(cd.getTitle());
			return ((Integer)this.getLength()).compareTo(cd.getLength());
		}
		return ((Integer)this.getTracks().size()).compareTo(cd.getTracks().size());
	}
}
