package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

public class Book extends Media implements Comparable<Object>{
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Long> wordFrequency = new TreeMap<>();
	
	public Book(String title){
		super(title);
	}
	
	public Book(String title, String category){
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors){
		super(title, category);
		this.authors = authors;
		//TODO: check author condition
	}
	
	public Book(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}

	public List<String> getAuthors() {
		return authors;
	}

	public void setAuthors(List<String> authors) {
		this.authors = authors;
	}
	
	public void addAuthor(String authorName) {
		if(!this.authors.contains(authorName)) this.authors.add(authorName);
	}
	
	public void removeAuthor(String authorName) {
		if(this.authors.contains(authorName))
			for(int i = 0; i < this.authors.size(); i++)
				if(this.authors.get(i).equals(authorName)) this.authors.remove(i); 
	}
	
	public void processContent() {
		String[] contentStrings = this.content.split("\\P{LD}+");
		Arrays.sort(contentStrings);
		for(String token: contentStrings) {
			if(!contentTokens.contains(token)) contentTokens.add(token);
		}
		wordFrequency = Arrays.stream(contentStrings).collect(Collectors.groupingBy(s -> s, Collectors.counting()));
	}

	public void setContent(String content) {
		this.content = content;
		processContent();
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		StringBuilder book = new StringBuilder();
		book.append(this.getTitle() + " - ");
		book.append(this.getCategory() + " - ");
		book.append(this.getAuthors() + " - ");
		book.append(this.getCost() + "\n");
		book.append("Number of tokens: " + contentTokens.size() +"\n");
		book.append(wordFrequency.toString());
		return book.toString();
	}
}
