package hust.soict.globalict.aims.media;

public class Disc extends Media{
	private int length;
	private String director;

	public Disc(String title, String category, float cost) {
		super(title, category, cost);
		// TODO Auto-generated constructor stub
	}
	
	public Disc(String title, String category, float cost, int length) {
		super(title, category, cost);
		this.length = length;
	}

	public int getLength() {
		return length;
	}
	
	public String getDirector() {
		return director;
	}

	public void setLength(int length) {
		this.length = length;
	}

	public Disc(String title, String category) {
		super(title, category);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title) {
		super(title);
		// TODO Auto-generated constructor stub
	}

	public Disc(String title, int length, String director) {
		super(title);
		this.length = length;
		this.director = director;
	}

}
