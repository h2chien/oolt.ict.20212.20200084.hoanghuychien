package hust.soict.globalict.aims.order;
import java.util.ArrayList;
import java.util.Random;

import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.utils.MyDate;

public class Order {
	public static final int MAX_NUMBERS_ORDERED = 10;
	public static final int MAX_LIMITTED_ORDERS = 5;
	public static final int MAX_NUMBERS_LUCKY_ITEMS = 1;
	
	private static int nbOrders = 0;
	
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
	private MyDate dateOrdered;
	private int nbLuckyItems = 0;
	private String id;

	public Order(MyDate d) {
		if(nbOrders == MAX_LIMITTED_ORDERS) {
			System.out.println("Can not make more than " + MAX_LIMITTED_ORDERS + " orders!");
			this.dateOrdered = null;
		}else {
			this.dateOrdered = d;
			nbOrders++;
			id = "ORD" + nbOrders;
		}
	}
	public Order() {
		this(new MyDate());
	}
	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}
	
	public void addMedia(Media media) {
		if(itemsOrdered.size() == MAX_LIMITTED_ORDERS) {
			System.out.println("The order list is full.");
		}else {
			itemsOrdered.add(media);
			System.out.println("Have added! (item ID: " + media.getId() +")");
		}
	}
	
	public String getId() {
		return id;
	}
	public void removeMedia(String id) {
		boolean exist = false;
			for(int i = 0; i < itemsOrdered.size(); i++)
				if(itemsOrdered.get(i).getId().equals(id)) {
					exist = true;
					itemsOrdered.remove(i);
					System.out.println("Have removed!");
					break;
				}
		if(!exist) {
			System.out.println("The item is not on the order list!");
		}
	}
	
	public float totalCost() {
		float total = 0;
		for(Media d: itemsOrdered) {
			if(d != null && !d.isLucky()) total += d.getCost();
		}
		return total;
	}
	
	public void printOrder() {
		if(this.getDateOrdered() == null) {System.out.println("There is no more order!"); return;}
		System.out.printf("\n******************Order ID: %s*******************\n", this.id);
		System.out.print("Date: ");
		this.getDateOrdered().print();
		System.out.println("Ordered items:");
		for(int i = 0; i < itemsOrdered.size(); i++) {
			System.out.print((i+1) + ". " + itemsOrdered.get(i).getId() + " - " + itemsOrdered.get(i).getTitle() + " - " + itemsOrdered.get(i).getCategory() + " - " 
								+ ": " + itemsOrdered.get(i).getCost() + "$");
			if(itemsOrdered.get(i).isLucky()) System.out.print(" - Lucky item");
			System.out.println();
		}
		System.out.println("Total cost: " + this.totalCost());
		System.out.println("***************************************************\n");
	}
	
	public static int getNbOrders() {
		return nbOrders;
	}
	public Media getALuckyItem() {
		if(nbLuckyItems == itemsOrdered.size() || nbLuckyItems == MAX_NUMBERS_LUCKY_ITEMS) return null;
		Random r = new Random();
		int randInt = r.nextInt(itemsOrdered.size());
		if(!itemsOrdered.get(randInt).isLucky()) {
			itemsOrdered.get(randInt).setLucky(true);
			nbLuckyItems++;
			return itemsOrdered.get(randInt);
		}else {
			return getALuckyItem();
		}
	}
	
	public int getNbItems() {
		return this.itemsOrdered.size();
	}
}
