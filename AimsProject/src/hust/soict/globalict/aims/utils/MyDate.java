package hust.soict.globalict.aims.utils;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Month;
import java.time.format.TextStyle;
import java.util.Date;
import java.util.Locale;
import java.util.Scanner;

public class MyDate {
	private int day;
	private int month;
	private int year;
	
	public MyDate() {
		String[] date = LocalDate.now().toString().split("-");
		this.day = Integer.valueOf(date[2]);
		this.month = Integer.valueOf(date[1]);
		this.year = Integer.valueOf(date[0]);
	}
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String date) {
		String[] d = date.split(" ");
		this.year = Integer.valueOf(d[2]);
		this.month = Month.valueOf(d[0].toUpperCase()).getValue();
		this.day = Integer.valueOf(d[1].replaceAll("[a-z]", ""));
	}
	
	public MyDate(String day, String month, String year) {
		String[] dayStrings = {"first", "second", "third", "fourth", "fifth", "sixth",
							   "seventh", "eighth", "ninth", "tenth", "eleventh",
							   "twelfth", "thirteenth", "fourteenth", "fifteenth",
							   "sixteenth", "seventeenth", "eighteenth", "nineteenth", "twentieth"
							};
		if(day.equals("thirtieth")) this.day = 30;
		if(day.equals("thirty-first"))this.day = 31;
		for(int i = 0; i < dayStrings.length; i++) {
			if(day.equals(dayStrings[i])) {
				this.day = i + 1;
				break;
			}
			if(day.equals("twenty " + dayStrings[i]) || day.equals("twenty-" + dayStrings[i])) {
				this.day = i + 21;
				break;
			}
		}
		this.month = Month.valueOf(month.toUpperCase()).getValue();
		String[] yearStrings = {"one", "two", "three", "four", "five",
								"six", "seven", "eight", "nine", "ten",
								"eleven", "twelve", "thirteen", "fourteen", "fifteen",
								"sixteen", "seventeen", "eighteen", "nineteen", "twenty"
							   };
		String[] yearStrings2 = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
		String[] strs = year.split(" ");
		if(strs.length == 2 || strs.length == 3) {
			for(int i = 0; i < yearStrings.length; i++) {
				if(strs[0].equals(yearStrings[i])) {
					this.year = (i + 1)*100;
					break;
				}
			}
			if(strs[1].equals("thousand")) {
				this.year *= 10;
			}
			else if(!strs[1].equals("hundred")) {
				for(int i = 0; i < yearStrings.length; i++) {
					if(strs[strs.length-1].equals(yearStrings[i])) {
						this.year += i + 1;
						break;
					}
				}
				outer:
				for(int i = 0; i < yearStrings2.length; i++) {
					if(strs[1].equals(yearStrings2[i])) {
						this.year += (i + 2)*10;
						break outer;
					}else {
						for(int j = 0; j < 9; j++) {
							if(strs[1].equals(yearStrings2[i] + "-" + yearStrings[j])) {
								this.year += (i + 2)*10 + j + 1;
								break outer;
							}
						}
					}
				}
			}
		}
		if(strs.length == 4) {
			for(int i = 0; i < 10; i++) {
				if(strs[3].equals(yearStrings[i])) {
					this.year = 2001 + i;
				}
			}
		}
	}

	public int getDay() {
		return day;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getYear() {
		return year;
	}

	public void setDay(int day) {
		this.day = day;
	}
	
	public void setDay(String day) {
		this.day = Integer.valueOf(day.replaceAll("[a-z]", ""));
	}

	public void setMonth(int month) {
		this.month = month;
	}
	
	public void setMonth(String month) {
		this.month = Month.valueOf(month.toUpperCase()).getValue();
	}

	public void setYear(int year) {
		this.year = year;
	}
	
	public void accept() {
		System.out.print("Input date (MM/DD/YYYY): ");
		try (Scanner input = new Scanner(System.in)) {
			String date = input.nextLine();
			String[] dateArr = date.split("/");
			this.setDay(dateArr[1]);
			this.setMonth(Integer.valueOf(dateArr[0]));
			this.setYear(Integer.valueOf(dateArr[2]));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void print() {
		switch (this.getDay()){
		case 1, 21, 31:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " " + this.getDay() + "st " + this.getYear());
			break;
		case 2, 22:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " " + this.getDay() + "nd " + this.getYear());
			break;
		case 3, 23:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " " + this.getDay() + "rd " + this.getYear());
			break;
		default:
			System.out.println(Month.of(this.getMonth()).getDisplayName(TextStyle.FULL_STANDALONE, Locale.ENGLISH) + " " + this.getDay() + "th " + this.getYear());
			break;
		}
	}
	
	public String returnFormat(String form) {
		SimpleDateFormat stdFormat = new SimpleDateFormat("MM-dd-yyyy");
		Date date = null;
		try {
			date = stdFormat.parse("" + this.getMonth() + "-" + this.getDay() + "-" + this.getYear());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		SimpleDateFormat formatter = new SimpleDateFormat(form);
		return formatter.format(date);
	}
	
	public void print(String form) {
		System.out.println(returnFormat(form));
	}
}
