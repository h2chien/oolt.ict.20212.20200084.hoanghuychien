package dice;

import java.util.HashMap;
import java.util.Map;

public class Dice {
	private Map<Integer, Float> probability = new HashMap<Integer, Float>();
	
	public Dice() {
		// TODO Auto-generated constructor stub
		this.probability = Map.of(1, 1.0f/6, 2, 1.0f/6, 3, 1.0f/6, 4, 1.0f/6, 5, 1.0f/6, 6, 1.0f/6);
	}
	
	public Dice(int n, float prob) {
		HashMap<Integer, Float> tmp = new HashMap<Integer, Float>();
		for(int i = 1; i <= 6; i++) {
			if(i == n) tmp.put(i, prob);
			else tmp.put(i, (1.0f - prob)/5);
		}
		this.probability = tmp;
	}

	public Map<Integer, Float> getProbability() {
		return probability;
	}

	public void setProbability(Map<Integer, Float> probability) {
		this.probability = probability;
	}
	
	public int roll() {
		float rand = (float)Math.random();
		float sum = 0;
		for(int i = 1; i <=6; i++) {
			sum += probability.get(i);
			if(sum >= rand) return i;
		}
		return 0;
	}
}
