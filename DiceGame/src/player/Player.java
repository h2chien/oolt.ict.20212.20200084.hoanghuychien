package player;

import dice.Dice;

public class Player {
	private String name;
	private int score;
	private static int nPlayer = 0;
	
	public Player() {
		// TODO Auto-generated constructor stub
		this("Player " + ++nPlayer, 0);
		if(nPlayer >= 4) nPlayer = 0;
	}
	
	public Player(String name, int score) {
		super();
		this.name = name;
		this.score = score;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public void chooseDice(Dice dice) {
		int plusScore = dice.roll();
		for(int i = 1; i <= 6; i++) {
			if(dice.getProbability().get(i) == 0.2f) {
				System.out.println("You have chosen dice number " + i + ".");
			}
		}
		System.out.println("You have rolled " + plusScore + "!");
		this.score += plusScore;
	}
	
	public void losing() {}
}
