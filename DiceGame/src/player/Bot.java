package player;

public class Bot extends Player {
	private String loseReaction;
	private static int nBot = 0;
	private static String[] loseReactionSet = {"No way!", "Bullshit!", "I can do better.", "You've got lucky."};

	public Bot() {
		this("Bot" + ++nBot, 0);
		// TODO Auto-generated constructor stub
		setLoseReaction(nBot);
		if(nBot >= 4) nBot = 0;
	}

	public Bot(String name, int score) {
		super(name, score);
		// TODO Auto-generated constructor stub
	}

	public String getLoseReaction() {
		return loseReaction;
	}

	public void setLoseReaction(String loseReaction) {
		this.loseReaction = loseReaction;
	}
	
	private void setLoseReaction(int n) {
		// TODO Auto-generated method stub
		this.loseReaction = loseReactionSet[n];
	}
	
	@Override
	public void losing() {
		// TODO Auto-generated method stub
		System.out.println(this.loseReaction);
	}
}
