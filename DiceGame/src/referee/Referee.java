package referee;

import java.util.Random;

import dice.Dice;
import player.Player;

public class Referee {
	private Player[] players;
	private Dice[] dices;
	private int currentTurn;
	private boolean endGame = false;
	
	public Referee(Player[] players, Dice[] dices) {
		super();
		this.players = players;
		this.dices = dices;
		this.currentTurn = 0;
	}

	public void nextPlayer() {
		System.out.println("----------------------------------------------------");
		System.out.printf("It's %s's turn. You have %d score now.\n", 
				players[currentTurn].getName(), players[currentTurn].getScore());
		int rand = Math.abs(new Random().nextInt() % dices.length);
		players[currentTurn].chooseDice(dices[rand]);
		this.markPlayer(players[currentTurn]);
		currentTurn++;
		if(currentTurn >= 4) currentTurn = 0;
	}
	
	public void markPlayer(Player player) {
		if(player.getScore() > 21) player.setScore(0);
		System.out.println("After this roll, you have " + player.getScore() + " score.\n");
		System.out.println("----------------------------------------------------");
		if(player.getScore() == 21) {
			this.endGame = true;
			System.out.println("----------------------------------------------------");
			System.out.printf("%s is the winner!\n\n", player.getName());
			for(int i = 0; i < players.length; i++) {
				if(players[i].getScore() != 21) {
					System.out.println(players[i].getName() + "'s reaction:");
					players[i].losing();
				}
			}
		}
	}

	public boolean isEndGame() {
		return endGame;
	}
	
}
