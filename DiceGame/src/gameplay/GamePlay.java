package gameplay;

import java.util.Scanner;

import dice.Dice;
import player.Bot;
import player.Player;
import referee.Referee;

public class GamePlay {
	public static void main(String[] args) {
		Player[] players = new Player[4];
		
		Scanner input = new Scanner(System.in);
		
		System.out.print("Enter number of real players: ");
		
		int nPlayer = input.nextInt(); input.nextLine();
		
		for(int i = 0; i < nPlayer; i++) {
			System.out.printf("Enter player %d's name: ", i+1);
			String name = input.nextLine();
			if(name != null) players[i] = new Player(name, 0);
			else players[i] = new Player();
		}
		System.out.println("\n----------------------------------------------------");
		
		for(int i = nPlayer; i < 4; i++) {
			players[i] = new Bot();
		}
		
		Dice[] dices = {new Dice(1, 0.2f), new Dice(2, 0.2f), new Dice(3, 0.2f), new Dice(4, 0.2f)};
		
		Referee referee = new Referee(players, dices);
		
		while(!referee.isEndGame()) {
			referee.nextPlayer();
		}
	}
}
