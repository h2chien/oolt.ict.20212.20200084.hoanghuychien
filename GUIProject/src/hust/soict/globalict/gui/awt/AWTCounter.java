package hust.soict.globalict.gui.awt;

import java.awt.*; 
import java.awt.event.*; 
	
public class AWTCounter extends Frame implements ActionListener {
	private Label lblCount;
	private TextField tfCount;
	private Button btnCount;
	private int count = 0;
	
	public AWTCounter () {
		setLayout(new FlowLayout());
		
		lblCount = new Label("Counter");
		add(lblCount);
		tfCount = new TextField(count + "", 10);
		tfCount.setEditable(false);
		add(tfCount);
		btnCount = new Button("Count");
		add(btnCount);
		btnCount.addActionListener(this);
		
		addWindowListener(new MyWindowListener());
	
		setTitle("AWT Counter");
		setSize(250, 100);
		
//		System.out.println(this);
//		System.out.println(lblCount);
//		System.out.println(tfCount);
//		System.out.println(btnCount);
		setVisible(true);
//		System.out.println(this);
//		System.out.println(lblCount);
//		System.out.println(tfCount);
//		System.out.println(btnCount);
	}
	
	public static void main(String[] args) {
	
		AWTCounter app = new AWTCounter();
	
	}
	
	@Override
	public void actionPerformed(ActionEvent evt) {
		++count;
		
		tfCount.setText(count + "");
	}
	
	private class MyWindowListener implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			System.exit(0);
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Iconified");
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Deiconified");
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Activated");
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Deactivated");
		}
		
	}
}
