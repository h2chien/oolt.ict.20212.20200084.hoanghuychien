package hust.soict.globalict.gui.awt;

import java.awt.*;
import java.awt.event.*;

public class MouseEventDemo extends Frame{
	private TextField tfMouseX;
	private TextField tfMouseY;
	
	public MouseEventDemo() {
		setLayout(new FlowLayout());
		
		add(new Label("X-Click: "));
		tfMouseX = new TextField(10);
		tfMouseX.setEditable(false);
		add(tfMouseX);
		
		add(new Label("Y-Click: "));
		tfMouseY = new TextField(10);
		tfMouseY.setEditable(false);
		add(tfMouseY);
		
		addMouseListener(new MyMouseListener());
		
		addWindowListener(new MyWindowListener());
		
		setTitle("MouseEvent Demo");
		setSize(350, 100);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		new MouseEventDemo();
	}
	
	private class MyMouseListener implements MouseListener{

		@Override
		public void mouseClicked(MouseEvent e) {
			// TODO Auto-generated method stub
			tfMouseX.setText(e.getX() + "");
	        tfMouseY.setText(e.getY() + "");
		}

		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	private class MyWindowListener implements WindowListener {

		@Override
		public void windowOpened(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowClosing(WindowEvent e) {
			// TODO Auto-generated method stub
			System.exit(0);
		}

		@Override
		public void windowClosed(WindowEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void windowIconified(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Iconified");
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Deiconified");
		}

		@Override
		public void windowActivated(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Activated");
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			// TODO Auto-generated method stub
			System.out.println("Window Deactivated");
		}
		
	}
}
