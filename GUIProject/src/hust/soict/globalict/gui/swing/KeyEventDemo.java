package hust.soict.globalict.gui.swing;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class KeyEventDemo extends JFrame{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JTextField tfInput;
	private JTextArea taDisplay;
	
	public KeyEventDemo() {
		Container cp = getContentPane();
		
		cp.setLayout(new FlowLayout());
		
		cp.add(new JLabel("Enter Text: "));
		tfInput = new JTextField(10);
		cp.add(tfInput);
		
		taDisplay = new JTextArea(5, 40);
		cp.add(taDisplay);
		
		tfInput.addKeyListener(new KeyListener() {
			
			@Override
			public void keyTyped(KeyEvent e) {
				// TODO Auto-generated method stub
				taDisplay.append("You have typed " + e.getKeyChar() + "\n");
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
		});
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("KeyEvent Demo");
		setSize(400, 200);
		setVisible(true);
	}
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				new KeyEventDemo();
			}
		});
	}
	
}
